/*
 * Advent of code 2018 - Day 2
 *
 * Author: Katerina V. - /u/kati256
 *
 * Sample input:
 *
 * abcdef | 0 0
 * bababc | 1 1
 * abbcde | 1 0
 * abcccd | 0 1
 * aabcdd | 1 0
 * abcdee | 1 0
 * ababab | 0 1
 *
 * Output: 4 x 3 = 12
 */

#include <iostream>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

template <typename T, typename Container>
const bool in(const Container &l, T n) {
        bool r = false;
	for (const auto e : l)
		r |= (e == n);
	return r;
}

const std::vector<std::string> splitfunc(const std::string &s) {
	std::vector<std::string> res;

	size_t first = 0;
	size_t last = 0;

	for (last = 0; last < s.length(); last++) {
		if (s[last] == ' ') {
			res.push_back(s.substr(first, last - first));
			first = last + 1;
		}
	}

	res.push_back(s.substr(first, last-first));
	return res;
}

const std::pair<int, int> checksum(const std::vector<std::string> &l) {
       int d = 0;
       int t = 0;
       bool id, it;
       for (auto s : l) {
	        id = false; it = false;
		std::vector<char> seen;
		for (char c : s) {
			if (in(seen, c)) {continue;}
			seen.push_back(c);
			int n = 0;
			for (char _c : s) { if (c == _c) n++;}
			if (!id && n == 2) {d++; id = true;}
			if (!it && n == 3) {t++; it = true;}
		}
	}

	return std::make_pair(d, t);
}

const int equal_chars(const std::string &s1, const std::string &s2) {
	// Assuming they are of equal length.
	int n = 0;
	for (int i = 0; i < s1.length(); i++)
		if(s1[i] == s2[i]) n++;
	return n;
}

void print_similar(const std::vector<std::string> &l) {
	for (int i = 0; i < l.size(); i++) {
		for (int j = i+1; j < l.size(); j++) {
			if (l[i].length() == l[j].length() &&
			    equal_chars(l[i], l[j]) == l[i].length() - 1)
				std::cout << l[i] << '\n' << l[j] << '\n';
		}
	}
}

int main() {

	std::cout << "Please enter an id list: " << '\n';
	std::string temp = " ";
	std::stringstream ss;
	while (temp.length() > 0) {
		std::getline(std::cin, temp);
		ss << temp << " ";
	}

	auto list = splitfunc(ss.str());
	auto res = checksum(list);
	std::cout << res.first << 'x' << res.second << ": " <<
		res.first * res.second << '\n';
	print_similar(list);
	return 0;
}
