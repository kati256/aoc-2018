/*
 * Advent of code 2018 - Day 1
 *
 * Author: Katerina V. - /u/kati256
 *
 *
 * Sample input:
 *
 * +1, +1, +1 results in  3
 * +1, +1, -2 results in  0
 * -1, -2, -3 results in -6
 *
 */

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

const std::vector<std::string> splitfunc(const std::string s) {
	std::vector<std::string> res;

	size_t first = 0;
	size_t last = 0;

	for (last = 0; last < s.length(); last++) {
		if (s[last] == ' ') {
			res.push_back(s.substr(first, last - first));
			first = last + 1;
		}
	}

	res.push_back(s.substr(first, last-first));
	return res;
}

const std::vector<int> clean(const std::vector<std::string> l) {

	std::vector<int> res;

	for (auto e : l) {
		std::string ns = e;
		if (ns[0] == '+')
			ns.erase(0, 1);
		if (ns[ns.length()-1] == ',')
			ns.erase(ns.length()-1);
		if (ns != "")
	 	        res.push_back(stoi(ns));
	}
	return res;
}

const bool in(const std::vector<int> l, int n) {
        bool r = false;
	for (auto e : l)
		r |= (e == n);
	return r;
}

const int sum(const std::vector<int> l) {
	int res = 0;
	bool doublefound = false;
	std::vector<int> seen;
	while (!doublefound) {
		for (auto e : l) {
			seen.push_back(res);
			res += e;
			doublefound = in(seen, res);
			if (doublefound) break;
		}
	}
	return res;
}

int main() {
	std::cout << "Please enter a space separated list: " << '\n';
	std::string temp = " ";
	std::stringstream ss;
	while (temp.length() > 0) {
		std::getline(std::cin, temp);
		ss << temp << " ";
	}

	std::cout << "The frequency is: "
		  << sum(clean(splitfunc(ss.str()))) << '\n';
	return 0;
}
